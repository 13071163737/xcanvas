/**
 * Created by haopeng on 2017/10/13.
 */
var WINDOW_HEIGHT = document.body.clientHeight;
var WINDOW_WIDTH = 1024;
var RADIUS = 8;
var MARGIN_TOP = 30;
var MARGIN_LEFT = 60;

window.onload = function(){
    var canvas = document.getElementById("canvas_time");
    var context = canvas.getContext("2d");

    canvas.width = WINDOW_WIDTH;
    canvas.height = WINDOW_HEIGHT;

    renderDateTime(context);
};

/**
 * 画出时间
 * @param ctx
 */
function renderDateTime(ctx) {
    var hours   = 00;
    var minutes = 00;
    var seconds = 07;

    renderDigit( MARGIN_LEFT , MARGIN_TOP , parseInt(hours/10) , ctx );
    renderDigit( MARGIN_LEFT + 15*(RADIUS+1) , MARGIN_TOP , parseInt(hours%10) , ctx );
    renderDigit( MARGIN_LEFT + 30*(RADIUS+1) , MARGIN_TOP , 10 , ctx );
    renderDigit( MARGIN_LEFT + 40*(RADIUS+1) , MARGIN_TOP , parseInt(minutes/10) , ctx);
    renderDigit( MARGIN_LEFT + 55*(RADIUS+1) , MARGIN_TOP , parseInt(minutes%10) , ctx);
    renderDigit( MARGIN_LEFT + 70*(RADIUS+1) , MARGIN_TOP , 10 , ctx);
    renderDigit( MARGIN_LEFT + 80*(RADIUS+1) , MARGIN_TOP , parseInt(seconds/10) , ctx);
    renderDigit( MARGIN_LEFT + 95*(RADIUS+1) , MARGIN_TOP , parseInt(seconds%10) , ctx);
}
/**
 * 画出一个数字
 * @param x
 * @param y
 * @param num
 * @param ctx
 */
function renderDigit(x, y, num, ctx) {
    ctx.fillStyle = "rgb(0,102,153)";
    for(var i = 0; i < digit[num].length; i++){
        for(var j = 0; j <digit[num][i].length; j++){
            if(digit[num][i][j] === 1){
                ctx.beginPath();
                ctx.arc(x + 2*j*(RADIUS+1)+(RADIUS+1), y+2*i*(RADIUS+1)+(RADIUS+1), RADIUS, 0, 2*Math.PI);
                ctx.closePath();
                ctx.fill();
            }
        }
    }
}